/****************************************************
 * Description: Service for t_meteor_cmdlog
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-05 reywong Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.meteor.websocket.service;


import ch.ethz.ssh2.Connection;

public interface WebsocketService {
    Connection getConnection(Long machineId) throws Exception;
}
